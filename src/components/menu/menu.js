import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Menu extends React.Component {

    componentDidMount() {
        console.log('pouet');
      }

    render() {
        return(
            <nav className="navbar sticky-top navbar-expand-lg custom-navbar">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav custom-navbar-nav">
                    <div className="d-flex justify-content-center">
                    <div className="p2"><li className="nav-item"><Link to="/" className="nav-link">Homes</Link></li></div>
                    <div className="p2"><li className="nav-item"><Link to="/category" className="nav-link">Category</Link></li></div>
                    <div className="p2"><li className="nav-item"><Link to="/products" className="nav-link">Products</Link></li></div>
                    </div>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Menu;