import React, { Component } from 'react';
import SocialNetworkLinksComponent from './socialNetworkLinks';
import LogoComponent from './logo';
import SearchComponent from './search';

class Header extends React.Component {

    render() {
        let currentPath = window.location.pathname
        return(
            <div>
            <div id="header-top" className="container">
                <div className="row row-eq-height">
                    <div className="col-4 align-self-center">
                        <SocialNetworkLinksComponent />
                    </div>
                    <div className="col-4 border-right border-left">
                        <LogoComponent />   
                    </div>
                    <div className="col-4 align-self-center">
                        <SearchComponent />
                    </div>
                </div>
            </div>
            
            </div>
        );
    }
}

export default Header;