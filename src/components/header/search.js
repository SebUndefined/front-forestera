import React, { Component } from 'react';

class SearchComponent extends Component {

    render() {
        return(
            <div id="search-top">
                <div class="d-flex justify-content-center">
                <form>
                    <div class="input-group mb-3 search-top-input">
                        <input type="text" class="form-control" placeholder="Search..." />
                        <span class="input-group-append">
                            <button class="btn btn-secondary" type="button"><i class="fas fa-search"></i></button>
                        </span>
                    </div>
                </form>
                </div>
            </div>
        );
    }
}

export default SearchComponent;