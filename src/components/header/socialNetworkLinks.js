import React, { Component } from 'react';

class SocialNetworkLinksComponent extends Component {

    render() {
        return(
            <div id="social-top">
                <div class="d-flex justify-content-center">
                    <a className="social-link"><i className="fab fa-facebook"></i></a>
                    <a className="social-link"><i className="fab fa-twitter"></i></a>
                    <a className="social-link"><i className="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        );
    }
}

export default SocialNetworkLinksComponent;