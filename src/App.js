import React, { Component } from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import Home from './components/home/home.js'
import SinglePost from './components/post/singlePost/singlePost.js'
import Menu from './components/menu/menu'
import Header from './components/header/header'
import {url} from './config/api.js';

import './App.css';
//Test

/* Category component */
const Category = () => (
  <div>
    <h2>Category</h2>
  </div>
)

//End test
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {post: ""};
  }

  componentDidMount() {
    this.PostList();

  }

  PostList() {
    // fetch('http://localhost/forestera/wp-json/wp/v2/posts')
    //     .then((response) => response.json())
    //     .then((responseJson) => this.setState({post: responseJson}));
        
  }

  render() {
    return (
      <div>
          <Header />
          <Menu />
           <Route exact={true} path="/" component={Home}/>
           <Route path="/category" component={Category}/>
           <Route path="/products" component={SinglePost}/>
      </div>
    );
  }
}

export default App;
